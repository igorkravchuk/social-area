﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialArea.Entities;
using SocialArea.Models;
using SocialArea.Services;
using SocialArea.ViewModels;

namespace SocialArea.Controllers
{
    [Produces("application/json")]
    [Route("api/People")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PeopleController : Controller
    {
        PeopleService peopleService;
        UserManager<ApplicationUser> userManager;

        public PeopleController(
            PeopleService peopleService,
            UserManager<ApplicationUser> userManager
        )
        {
            this.peopleService = peopleService;
            this.userManager = userManager;
        }

        [HttpGet]
        public async Task<List<Person>> Get()
        {
            var persons = await peopleService.All();
            return persons;
        }

        [HttpGet("{id}")]
        public async Task<Person> Get(string id)
        {
            var person = await peopleService.One(id);
            return person;
        }


        [HttpGet("me")]
        public async Task<Person> Me()
        {
            var user = await userManager.GetUserAsync(User);
            return user.Person;
        }

        [HttpGet("nearby")]
        public async Task<List<Person>> Nearby(
            double latitude, double longitude, int distance = 1000)
        {
            distance = 20000;
            var persons = await peopleService.NearbyPeople(latitude, longitude, distance);
            return persons;
        }

        [HttpPost("location")]
        public async Task<IActionResult> UpdateLocation([FromBody] Location location)
        {
            var user = await userManager.GetUserAsync(User);
            await peopleService.UpdateLocation(user.Person.PersonId, location);

            return Ok();
        }

        [HttpPost("profile")]
        public async Task<IActionResult> UpdateProfile([FromBody]Profile profile)
        {
            var user = await userManager.GetUserAsync(User);

            await peopleService.UpdateProfile(user.Person.PersonId, profile);

            return Ok();
        }
    }
}
