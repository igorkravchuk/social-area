﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialArea.Entities;
using SocialArea.Models;
using SocialArea.Services;
using SocialArea.ViewModels;

namespace SocialArea.Controllers
{
    [Produces("application/json")]
    [Route("api/Checkin")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CheckinController : Controller
    {
        CheckinService checkinService;
        UserManager<ApplicationUser> userManager;

        public CheckinController(
            CheckinService checkinService,
            UserManager<ApplicationUser> userManager
        )
        {
            this.checkinService = checkinService;
            this.userManager = userManager;
        }

        [HttpGet]
        public async Task<List<Checkin>> Get()
        {
            var result = await checkinService.All();
            return result;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CheckinViewModel checkIn)
        {
            if (checkIn == null)
                return BadRequest("Invalid Model");
            var user = await userManager.GetUserAsync(User);
            var createdDate = DateTime.UtcNow;
            var checkin = new Checkin
            {
                Location = new Location
                {
                    Latitude = checkIn.LocationLatitude,
                    Longitude = checkIn.LocationLangitude
                },
                Note = checkIn.Note,
                CheckinCategoryId = checkIn.CheckinCategoryId,
                Person = user.Person,
                FormattedAddress = checkIn.FormattedAddress,
                Created = createdDate
            };
            await checkinService.Add(checkin);
            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<Person> Get(string id)
        {
            var result = await checkinService.One(id);
            return result;
        }

        [HttpGet("Category")]
        public async Task<List<CheckinCategory>> GetAllCategories()
        {
            var result = await checkinService.AllCategories();
            return result;
        }

        //[HttpPost("Category")]
        //public async Task<IActionResult> Post(string categoryName)
        //{
        //    var result = await checkinService.AllCategories(id);
        //    return result;
        //}



    }
}