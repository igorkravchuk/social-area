﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SocialArea.Models;
using SocialArea.Services;
using SocialArea.ViewModels;

namespace SocialArea.Controllers
{
    [Produces("application/json")]
    [Route("[controller]/[action]")]
    public class AuthController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUserClaimsPrincipalFactory<ApplicationUser> claimsPrincipalFactory;
        private readonly IAuthenticationSchemeProvider authenticationSchemeProvider;
        private readonly IConfiguration configuration;
        private readonly PeopleService peopleService;

        public AuthController(
            UserManager<ApplicationUser> userManager,
            IUserClaimsPrincipalFactory<ApplicationUser> claimsPrincipalFactory,
            IAuthenticationSchemeProvider authenticationSchemeProvider,
            IConfiguration configuration,
            PeopleService peopleService
        )
        {
            this.userManager = userManager;
            this.claimsPrincipalFactory = claimsPrincipalFactory;
            this.authenticationSchemeProvider = authenticationSchemeProvider;
            this.configuration = configuration;
            this.peopleService = peopleService;
        }

        [HttpGet]
        public async Task<IActionResult> SignInAll()
        {
            var allSchemeProvider = (await authenticationSchemeProvider.GetAllSchemesAsync())
                .Select(n => n.DisplayName).Where(n => !String.IsNullOrEmpty(n));
            return Ok(allSchemeProvider);
        }

        [HttpGet]
        public IActionResult SignIn(string provider)
        {
            return Challenge(new AuthenticationProperties { RedirectUri = "/Auth/SignInCallback" }, provider);
        }

        [HttpGet]
        public async Task<IActionResult> SignInCallback()
        {
            var result = await HttpContext.AuthenticateAsync(IdentityConstants.ExternalScheme);

            var externalUserId = result.Principal.FindFirstValue("sub")
                                ?? result.Principal.FindFirstValue(ClaimTypes.NameIdentifier)
                                ?? throw new Exception("Cannot find external user id");

            var provider = result.Properties.Items[".AuthScheme"];

            var user = await userManager.FindByLoginAsync(provider, externalUserId);

            if (user == null)
            {
                var email = result.Principal.FindFirstValue("email")
                            ?? result.Principal.FindFirstValue(ClaimTypes.Email);
                if (email != null)
                {
                    user = await userManager.FindByEmailAsync(email);

                    if (user == null)
                    {
                        user = new ApplicationUser { UserName = email, Email = email };
                        await userManager.CreateAsync(user);
                    }

                    await userManager.AddLoginAsync(user,
                        new UserLoginInfo(provider, externalUserId, provider));
                }
            }

            if (user == null) return BadRequest();


            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            var claimsPrincipal = await claimsPrincipalFactory.CreateAsync(user);
            await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, claimsPrincipal);

            var token = CreateToken(user);

            if (user.Person == null)
            {
                await peopleService.Add(new Person
                {
                    UserId = await userManager.GetUserIdAsync(user),
                    Location = new Location(),
                    Profile = new Profile()
                });
            }

            return RedirectToAction("SignInSuccess", new { token = new JwtSecurityTokenHandler().WriteToken(token) });
        }

        [HttpGet]
        public IActionResult SignInSuccess(string token)
        {
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> SignOut()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            return Ok();
        }

        [HttpGet]
        public IActionResult Me()
        {
            var userId = userManager.GetUserId(User);

            if (User.Identity.IsAuthenticated)
            {
                return Json(new
                {
                    Name = User.Identity.Name,
                    Id = userId
                });
            } else
            {
                return Unauthorized();
            }
        }

        [HttpGet]
        public async Task<IActionResult> Im()
        {
            var user = await userManager.GetUserAsync(User);
            return Json(user);
        }

        [HttpPost]
        public async Task<IActionResult> Token([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByEmailAsync(model.UserName);

                if (user != null)
                {
                    var hasPassword = await userManager.HasPasswordAsync(user);

                    if (hasPassword) {
                        if (!await userManager.CheckPasswordAsync(user, model.Password))
                        {
                            return BadRequest("Username or Password is not correct");
                        }

                    } else
                    {
                        //var result = await userManager.AddPasswordAsync(user, model.Password);
                        //if (!result.Succeeded)
                        //{
                        //    return BadRequest(Json(result.Errors));
                        //}
                        return BadRequest("User has not password");
                    }

                    var token = CreateToken(user);
                    return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
                }

            }

            return BadRequest("Could not create token");
        }

        [HttpPost]
        public async Task<IActionResult> Registration([FromBody] LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await userManager.FindByEmailAsync(model.UserName);

                    if (user != null)
                    {
                        return BadRequest("Can not create user");
                    }

                    user = new ApplicationUser
                    {
                        UserName = model.UserName,
                        Email = model.UserName
                    };

                    var result = await userManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        await peopleService.Add(new Person
                        {
                            UserId = await userManager.GetUserIdAsync(user),
                            Location = new Location(),
                            Profile = new Profile()
                        });

                        var token = CreateToken(user);
                        return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
                    }
                    else
                    {
                        return BadRequest(Json(result.Errors));
                    }

                }

                return BadRequest("Could not create token");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        private JwtSecurityToken CreateToken(ApplicationUser user)
        {
            var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Authentication:SigningKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            return new JwtSecurityToken
            (
                issuer: configuration["Authentication:Issuer"],
                audience: configuration["Authentication:Audience"],
                claims: claims,
                expires: DateTime.UtcNow.AddDays(60),
                notBefore: DateTime.UtcNow,
                signingCredentials: creds
            );            
        }
    }
}