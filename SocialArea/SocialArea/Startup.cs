﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SocialArea.Entities;
using SocialArea.Models;
using SocialArea.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace SocialArea
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>();

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
                {
                    options.Password.RequiredLength = 4;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireDigit = false;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddScoped<DistanceService>();
            services.AddScoped<PeopleService>();
            services.AddScoped<CheckinService>();
            services.AddScoped<IUserStore<ApplicationUser>,
                UserOnlyStore<ApplicationUser, ApplicationDbContext>>();

            services.AddAuthentication(options =>
                {
                    //options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    //options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    //options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                })
                .AddFacebook(facebookOptions =>
                {
                    facebookOptions.AppId = Configuration.GetSection("Authentication")["FacebookAppId"];
                    facebookOptions.AppSecret = Configuration.GetSection("Authentication")["FacebookAppSecret"];
                    facebookOptions.Scope.Add("public_profile");
                    facebookOptions.Scope.Add("email");
                    facebookOptions.SignInScheme = IdentityConstants.ExternalScheme;

                    //facebookOptions.Events = new OAuthEvents
                    //{
                    //    OnCreatingTicket = context => {
                    //        // Use the Facebook Graph Api to get the user's email address
                    //        // and add it to the email claim

                    //        //var client = new FacebookClient(context.AccessToken);
                    //        //dynamic info = client.Get("me", new { fields = "name,id,email" });

                    //        //context.Identity.AddClaim(new Claim(ClaimTypes.Email, info.email));
                    //        return Task.FromResult(0);
                    //    }
                    //};
                })
                .AddInstagram(instagramOptions =>
                {
                    instagramOptions.ClientId = Configuration["Authentication:InstagramClientId"];
                    instagramOptions.ClientSecret = Configuration["Authentication:InstagramClientSecret"];
                    instagramOptions.SignInScheme = IdentityConstants.ExternalScheme;
                    instagramOptions.Scope.Add("basic");
                    instagramOptions.Scope.Add("public_content");
                })
                .AddCookie()
                .AddJwtBearer(jwtBearerOptions =>
                {
                    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                    {
                        //ValidateActor = true,
                        //ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Authentication:Issuer"],
                        ValidAudience = Configuration["Authentication:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Authentication:SigningKey"]))
                    };
                });



            services.AddMvc()
                .AddJsonOptions(options => {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });

            services.AddSwaggerGen(options => {
                options.SwaggerDoc("v1", new Info { Title = "Social Area Api", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ApplicationDbContext dbContext
            )
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Contacts API V1");
            });

            //dbContext.Database.EnsureCreated();
        }
    }
}
