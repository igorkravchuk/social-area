﻿using Microsoft.EntityFrameworkCore;
using SocialArea.Entities;
using SocialArea.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialArea.Services
{
    public class CheckinService
    {
        ApplicationDbContext _db;

        public CheckinService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<Checkin>> All()
        {
            return await _db.Checkins
                .ToListAsync();
        }

        public async Task<Person> One(string personId)
        {
            return await _db.Persons
                .FirstOrDefaultAsync(p => p.PersonId == personId);
        }

        public async Task Add(Checkin checkin)
        {
            var result = _db.Add(checkin);
            await _db.SaveChangesAsync();
        }


        //* Categories

        public async Task<List<CheckinCategory>> AllCategories()
        {
            return await _db.CheckinCategories
                .ToListAsync();
        }
    }
}
