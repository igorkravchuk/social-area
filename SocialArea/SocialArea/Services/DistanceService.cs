﻿using SocialArea.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialArea.Services
{
    public class DistanceService
    {

        public double ClosestDistance(
            double point1Lat, double point1Long,
            double point2Lat, double point2Long
        )
        {
            var adr = (point1Lat * Math.PI) / 180;
            var afr = (point2Lat * Math.PI) / 180;
            var bdr = (point1Long * Math.PI) / 180;
            var bfr = (point2Long * Math.PI) / 180;
            var R = 6371210;
            var dist = Math.Acos(Math.Sin(adr) * Math.Sin(afr) + Math.Cos(adr) * Math.Cos(afr) * Math.Cos(bdr - bfr)) * R;
            return dist;
        }
    }
}
