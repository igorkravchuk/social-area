﻿using Microsoft.EntityFrameworkCore;
using SocialArea.Entities;
using SocialArea.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialArea.Services
{
    public class PeopleService
    {
        ApplicationDbContext _db;
        DistanceService _distanceService;

        public PeopleService(ApplicationDbContext db, DistanceService distanceService)
        {
            _db = db;
            _distanceService = distanceService;
        }

        public async Task<List<Person>> All()
        {
            return await _db.Persons
                //.Include(p => p.Location)
                //.Include(p => p.Profile)
                .ToListAsync();
        }

        public async Task<Person> One(string peopleId)
        {
            return await _db.Persons
                .FirstOrDefaultAsync(p => p.PersonId == peopleId);
        }

        public async Task Add(Person person)
        {
            var result = _db.Add(person);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateLocation(string personId, Location location)
        {
            var person = _db.Persons
                //.Include(p => p.Location)
                .FirstOrDefault(p => p.PersonId == personId);

            if (person?.Location == null)
            {
                person.Location = new Location();
            }

            person.Location.Updated = DateTime.Now;
            person.Location.Latitude = location.Latitude;
            person.Location.Longitude = location.Longitude;
            _db.Update(person);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateProfile(string personId, Profile profile)
        {
            var person = _db.Persons
                //.Include(p => p.Profile)
                .FirstOrDefault(p => p.PersonId == personId);

            if (person?.Profile == null)
            {
                person.Profile = new Profile();
            }

            person.Profile.Name = profile.Name;
            person.Profile.Gender = profile.Gender;
            person.Profile.PictureUrl = profile.PictureUrl;
            person.Profile.Facebook = profile.Facebook;
            person.Profile.Instagram = profile.Instagram;
            _db.Update(person);
            await _db.SaveChangesAsync();
        }

        public async Task<List<Person>> NearbyPeople(double latitude, double longitude, int distance)
        {
            var persons = await _db.Persons.Where(p =>
                _distanceService.ClosestDistance(p.Location.Latitude, p.Location.Longitude, latitude, longitude) < distance
                && DateTime.Now.Subtract(p.Location.Updated).TotalMinutes <= 30)
                //.Include(p => p.Location)
                //.Include(p => p.Profile)
                .ToListAsync();
            persons.ForEach(p =>
            {
                p.Distance = System.Convert.ToInt32(_distanceService.ClosestDistance(p.Location.Latitude, p.Location.Longitude, latitude, longitude));
            });
            return persons;
        }
    }
}
