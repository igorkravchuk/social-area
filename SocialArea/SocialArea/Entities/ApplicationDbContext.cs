﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SocialArea.Models;

namespace SocialArea.Entities
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private IConfiguration configuration;

        public DbSet<Person> Persons { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Checkin> Checkins { get; set; }
        public DbSet<CheckinCategory> CheckinCategories { get; set; }

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options,
            IConfiguration configuration
        ) : base(options)
        {
            this.configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            //.UseSqlServer(@"Server=.\SQLEXPRESS;Database=SocialDB;Trusted_Connection=True; User ID=sa;Password=tateeda1;");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<CheckinCategory>().HasData(
                new CheckinCategory
                {
                    CheckinCategoryId = Guid.NewGuid().ToString(),
                    Name = "Relax"
                },
                new CheckinCategory
                {
                    CheckinCategoryId = Guid.NewGuid().ToString(),
                    Name = "Food"
                },
                new CheckinCategory
                {
                    CheckinCategoryId = Guid.NewGuid().ToString(),
                    Name = "Fun"
                });
        }
    }
}
