﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialArea.ViewModels
{
    public class CheckinViewModel
    {
        public string CheckinCategoryId { get; set; }
        public double LocationLatitude { get; set; }
        public double LocationLangitude { get; set; }
        public string Note { get; set; }
        public string FormattedAddress { get; set;}
    }
}
