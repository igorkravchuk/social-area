﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialArea.Models
{
    public class Location
    {
        public string LocationId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime Updated { get; set; }
        
        public virtual Person Person { get; set; }
        public string PersonId { get; set; }
    }
}
