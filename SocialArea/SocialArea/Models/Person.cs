﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace SocialArea.Models
{
    public class Person
    {
        public string PersonId { get; set; }

        [JsonIgnore]
        public virtual ApplicationUser User { get; set; }
        public string UserId { get; set; }
        
        public virtual Location Location { get; set; }
        public virtual Profile Profile { get; set; }

        [NotMapped]
        public double Distance { get; set; }
    }
}
