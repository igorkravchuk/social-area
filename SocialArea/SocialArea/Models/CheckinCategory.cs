﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialArea.Models
{
    public class CheckinCategory
    {
        public string CheckinCategoryId { get; set; }
        public string Name { get; set; }
    }
}
