﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialArea.Models
{
    public class Checkin
    {
        public string CheckinId { get; set; }
        public virtual Location Location { get; set; }
        public virtual Person Person { get; set; }
        public string FormattedAddress { get;set; }

        [MaxLength(180)]
        public string Note { get; set; }
        public DateTime Created { get; set; }

        public string CheckinCategoryId { get; set; }
        public virtual CheckinCategory CheckinCategory { get; set; }

    }
}
