﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SocialArea.Models
{
    public class Profile
    {
        public string ProfileId { get; set; }

        [NotMapped]
        public string UserName {
            get{
                return Person?.User?.UserName;
            }
        }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string PictureUrl { get; set; }
        
        public virtual Person Person { get; set; }
        public string PersonId { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
    }
}
