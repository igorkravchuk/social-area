import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { DropDownModule } from "nativescript-drop-down/angular";
import { AppRoutingModule, COMPONENTS } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CoreModule } from "./core/core.module";

import { PeopleRepository } from "./repositories/people.repository";
import { CheckinRepository } from "./repositories/checkin.repository";

import { MapPreviewComponent } from "~/components/map-preview/map-preview.component";
import { AuthorizationService } from "~/services/authorization.service";
import { LocalStorageService } from "~/services/local-storage.service";
import { ApiService } from "~/services/api.service";
import { IdentityService } from "~/services/identity.service";
import { LocationService } from "~/services/location.service";
import { PeopleService } from "~/services/people.service";

import * as platform from "platform";
import { UrlService } from "~/services/url.service";

import { registerElement } from "nativescript-angular/element-registry";
import { StorageService } from "~/services/storage.service";
import { CheckinService } from "~/services/checkin.service";
registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView);

import { FilterByPipe } from "~/pipes/filter-by.pipe";
import { OrderByPipe } from "~/pipes/order-by.pipe";
import { TimerService } from "~/services/timer.service";
import { NavigationService } from "~/services/navigation.service";


@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        CoreModule,
        NativeScriptHttpClientModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        DropDownModule
    ], 
    declarations: [
        AppComponent,
        MapPreviewComponent,      

        FilterByPipe,
        OrderByPipe,
        ...COMPONENTS
    ],
    providers: [
        PeopleRepository,
        CheckinRepository,

        ApiService,
        AuthorizationService,
        LocalStorageService,
        IdentityService,
        LocationService,
        PeopleService,
        StorageService,
        UrlService,
        CheckinService,
        TimerService,
        NavigationService,
        FilterByPipe,
        OrderByPipe
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
