import { Injectable } from "@angular/core";
import { Person } from "../models/Person";
import { ApiService } from "../services/api.service";
import { Observable } from "rxjs";
import { LocalStorageService, LocalStorageKeys } from "~/services/local-storage.service";

interface IAuthResponse {
    token: string;
}

@Injectable()
export class PeopleRepository {

    constructor(
        private apiService: ApiService,
        private localStorage: LocalStorageService
    ) { }

    public getNearbyPeople(latitude: number | string, longitude : number | string): Promise<Person[]> {
        let params = {
            latitude: latitude,
            longitude: longitude,
            distance: 1000
        };
        return this.apiService.get<Person[]>("/api/people/nearby", params).toPromise();
    }

    public getPerson(id: string): Promise<Person> {
        return this.apiService.get<Person[]>("/api/people/"+id).toPromise();
    }
    
    public updateLocation(latitude: number | string, longitude : number | string): Promise<any> {
        let userId = this.localStorage.getItem(LocalStorageKeys.USER);
        let params = {
            latitude: latitude,
            longitude: longitude,
            distance: 1000
        };
        return this.apiService.post<any>("/api/People/location", params).toPromise();
    }

    public start(instaName: string) {
        let params = {
            instaName: instaName
        };
        return this.apiService.post<Person>("/api/People", params).toPromise();
    }

    public login(userName: string, password: string) {
        let params = {
            userName: userName,
            password: password
        }
        return this.apiService.post<IAuthResponse>("/auth/token", params).toPromise();
    }

    public register(userName: string, password: string) {
        let params = {
            userName: userName,
            password: password
        }
        return this.apiService.post<IAuthResponse>("/auth/registration", params).toPromise();
    }

    public getCurrentUser(): Promise<Person> {
        return this.apiService.get<Person>("/api/people/me").toPromise();
    }

    public editUserProfile(user: Person) {
        let profile = {
            name: user.profile.name,
            gender: user.profile.gender,
            instagram: user.profile.instagram,
            facebook: user.profile.facebook
        }
        return this.apiService.post<any>("/api/people/profile", profile).toPromise();
    }
}