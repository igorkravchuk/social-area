import { Injectable } from "@angular/core";
import { Checkin } from "../models/Checkin";
import { CheckinCategory } from "../models/CheckinCategory"
import { ApiService } from "../services/api.service";

export class CheckinRequest {
    locationLatitude: number;
    locationLangitude: number;
    note: string;
    formattedAddress: string;
    checkinCategoryId: string;
}

@Injectable()
export class CheckinRepository {
    constructor(
        private apiService: ApiService
    ){}

    createCheckin(checkIn: CheckinRequest){
        return this.apiService.post<Checkin>("/api/Checkin", checkIn).toPromise();
    }

    getCheckinsList(){
        return this.apiService.get<Checkin>("/api/Checkin").toPromise();
    }
    
    getCheckinCategories() {
        return this.apiService.get<CheckinCategory[]>("/api/Checkin/category").toPromise();
    }
}