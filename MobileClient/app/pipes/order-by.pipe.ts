import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'orderBy',
    pure: true
})

export class OrderByPipe implements PipeTransform {
    transform(array, orderBy, reverse = false) {
        if (!array || !orderBy) return array;

        let result = Array.from(array).sort((item1: any, item2: any) => {
            return this.orderByComparator(item1[orderBy], item2[orderBy]);
        });

        if (reverse) {
            return result.reverse();
        }

        return result;

    }

    orderByComparator(a: any, b: any): number {
        if (typeof a === 'string' && typeof b === 'string') {
            return this.defaultCompare(a.toLowerCase(), b.toLowerCase());
        }

        // if (typeof a === 'number' && typeof b === 'number') {
        //     return this.defaultCompare(a, b);
        // }

        return this.defaultCompare(a, b);

    }

    defaultCompare(a: any, b: any):number {
        if (a === b) {
            return 0;
        }
        if (a == null) {
            return 1;
        }
        if (b == null) {
            return -1;
        }
        return a > b ? 1 : -1;
    }
}