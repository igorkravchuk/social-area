import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'filterBy',
    pure: true
})
export class FilterByPipe implements PipeTransform {
    transform(array: any[], predicate: object | Function) {
        if (!array || !predicate) return array;

        if (typeof predicate === 'function')
            return array.filter(item => predicate(item));

        let keys = Object.keys(predicate);
        
        return array.filter((item) => 
            keys.some(key => {
                let itemValue = key.split('.').reduce((nested, key) => nested[key], item);
                let predicateValue = predicate[key];

                if (typeof predicateValue === 'function')
                    return predicateValue(itemValue);

                if (itemValue == undefined || predicateValue == undefined)
                    return false;

                if (Array.isArray(predicateValue))
                    return predicateValue.indexOf(itemValue) >= 0;

                return itemValue === predicateValue
            })
        );
    }
}