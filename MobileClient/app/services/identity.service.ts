import { Injectable } from "@angular/core";
import { Person } from "../models/Person";
import { LocalStorageService, LocalStorageKeys } from "./local-storage.service";


@Injectable()
export class IdentityService {
    private _accessToken: string;
    private _refreshToken: string;

    constructor(
        private localStorage: LocalStorageService
    ) {
        this._accessToken = this.localStorage.getItem(LocalStorageKeys.ACCESS_TOKEN);
        this._refreshToken = this.localStorage.getItem(LocalStorageKeys.REFRESH_TOKEN);
    }

    get isExist(): boolean {
        return !!this.accessToken;
    }

    get accessToken(): string {
        if (!this._accessToken) {
            this._accessToken = this.localStorage.getItem(LocalStorageKeys.ACCESS_TOKEN);
        }
        return this._accessToken;
    }
    set accessToken(val: string) {
        this.localStorage.setItem(LocalStorageKeys.ACCESS_TOKEN, val);
        this._accessToken = val;
    }

    get refreshToken(): string {
        if (!this._refreshToken) {
            this._refreshToken = this.localStorage.getItem(LocalStorageKeys.REFRESH_TOKEN);
        }
        return this._refreshToken;
    }
    set refreshToken(val: string) {
        this.localStorage.setItem(LocalStorageKeys.REFRESH_TOKEN, val);
        this._refreshToken = val;
    }

    get user(): Person {
        const user = this.localStorage.getItem(LocalStorageKeys.USER);
        return user ? JSON.parse(user) : new Person();
        
    }

    set user(user: Person) {
        this.localStorage.setItem(LocalStorageKeys.USER, JSON.stringify(user));
    }

    public clear(): void {
        this._refreshToken = null;
        this._accessToken = null;
        this.localStorage.clear();
    }
}