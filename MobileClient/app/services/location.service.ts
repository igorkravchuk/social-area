import { Injectable } from "@angular/core";
import { isEnabled, enableLocationRequest, getCurrentLocation, watchLocation, distance, clearWatch } from "nativescript-geolocation";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable()
export class LocationService {
    constructor(
         public httpClient: HttpClient
    ){}

    public enable(): Promise<any> {
        return isEnabled().then(function (isEnabled) {
            if (!isEnabled) {
                return enableLocationRequest().then(function () {
                }, function (e) {
                    console.log("Error: " + (e.message || e));
                    return Promise.reject(e);
                });
            }
        }, function (e) {
            console.log("Error: " + (e.message || e));
        });
    }

    public currentLocation(): Promise<any> {
        // return Promise.resolve({latitude: 46.450206, longitude: 30.746102});
        return this.enable().then(function () {
            let params = {
                desiredAccuracy: 1,
                updateDistance: 10,
                maximumAge: 20000,
                timeout: 20000
            }
            return getCurrentLocation(params);
        }, function () {
            return Promise.reject("You don't have geolocation permission");
        }) 
    }

    public getFormettedAddress(): Promise<any> {
        
        return this.currentLocation().then((loc) => {
            let absoluteUrl = "https://maps.googleapis.com/maps/api/geocode/json";
            let params = {
                address: loc.latitude + "," + loc.longitude,
                key: "AIzaSyD-PpBLFfK15vBphegbo5E7HcHHTdEQ8Ng"
            }
            return this.httpClient.get(absoluteUrl, {params: params})
                .pipe(map( (e: any) => { 
                    if (e.results) {
                        return e.results;
                    }
                    return;
                }))
                .toPromise();
        }).catch((error: any) => {
            alert(error);
            return Promise.reject(error);
        })
    }

    public getFormattedAddressByLatLong(latitude: number, longitude: number): Promise<any> {

        let absoluteUrl = "https://maps.googleapis.com/maps/api/geocode/json";
            let params = {
                address: latitude + "," + longitude,
                key: "AIzaSyD-PpBLFfK15vBphegbo5E7HcHHTdEQ8Ng"
            }

        return this.httpClient.get(absoluteUrl, {params: params})
            .pipe(map((e: any) => {
                if (e.results) {
                    return e.results.formatted_address;
                }
            })).toPromise();
    }
}