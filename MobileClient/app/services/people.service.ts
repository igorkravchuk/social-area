import { Injectable } from "@angular/core";
import { isEnabled, enableLocationRequest, getCurrentLocation, watchLocation, distance, clearWatch } from "nativescript-geolocation";
import { LocationService } from "~/services/location.service";
import { PeopleRepository } from "~/repositories/people.repository";
import { StorageService } from "~/services/storage.service";
@Injectable()
export class PeopleService {
    constructor(
        private locationService: LocationService,
        private peopleRepository: PeopleRepository,
        private storageService: StorageService
    )
    {}

    public updateMyLocation() {
        return this.locationService.currentLocation()
            .then((loc) => {
                this.storageService.updateCurrentLocation(loc);
                return this.peopleRepository.updateLocation(loc.latitude, loc.longitude)
            });
    }

    public updatePersonsList() {
        return this.locationService.currentLocation()
            .then((loc) => this.peopleRepository.getNearbyPeople(loc.latitude, loc.longitude))
            .then((persons) => {
                this.storageService.updatePersons(persons);
            })
    }
    
}