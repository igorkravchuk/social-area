import { Injectable } from "@angular/core";
import * as appSettings from "application-settings";

export enum LocalStorageKeys {
    ACCESS_TOKEN = "accessToken",
    REFRESH_TOKEN = "refreshToken",
    USER = "authorizedUser",
    PUSH_TOKEN = "pushToken"
}

@Injectable()
export class LocalStorageService {
    public setItem(key: LocalStorageKeys, value: string) {
        appSettings.setString(key, value);
    }

    public getItem(key: LocalStorageKeys): string {
        return appSettings.getString(key);
    }

    public clear() {
        Object.keys(LocalStorageKeys).forEach(key => appSettings.remove(LocalStorageKeys[key]));
    }
}