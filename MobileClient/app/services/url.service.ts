import { Injectable } from "@angular/core";
import { identity } from "rxjs";

@Injectable()
export class UrlService {

    public parseParams(url: string): any {
        var params = {}, 
            queryString = url.substring(1),
            regex = /([^&=]+)=([^&]*)/g,
            m;
            
        while(m = regex.exec(queryString)) {
            params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
        }
        
        return params;
    }

    public parseBaseUrl(url: string) {
        return url.replace(/\?.*/, '');
    }
}