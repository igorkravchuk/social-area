import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { StorageService } from "~/services/storage.service";
import { Observable, Subject } from "rxjs";


@Injectable()
export class NavigationService {
    constructor(){

    }
    private navigateSubject$ = new Subject<number>();

    public get navigate(): Observable<number> {
        return this.navigateSubject$.asObservable();
    }
    public changeDashboardPage(pageNumber: number) {
        return this.navigateSubject$.next(pageNumber);
    }
}