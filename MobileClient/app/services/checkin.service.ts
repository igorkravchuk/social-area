import { Injectable } from "@angular/core";
import { Checkin } from "~/models/Checkin";
import { StorageService } from "~/services/storage.service";
import { CheckinRepository, CheckinRequest } from "~/repositories/checkin.repository";

@Injectable()
export class CheckinService {
    constructor(
        private checkinRepository: CheckinRepository,
        private storageService: StorageService
    ){}

    createCheckin(params: CheckinRequest) {
        return this.checkinRepository.createCheckin(params);
    }
    
    updateChekinsList(){
        return this.checkinRepository.getCheckinsList()
                .then((checkins: any) => {
                    this.storageService.updateCheckins(checkins);
                })
    }

    updateCheckinCategoriesList() {
        return this.checkinRepository.getCheckinCategories()
                .then((categories: any) => {
                    this.storageService.updateCheckinCategories(categories);
                })
    }
}