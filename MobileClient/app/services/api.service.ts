import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders, HttpResponse, HttpEvent, HttpErrorResponse, HttpParams } from "@angular/common/http";
import {map, catchError, share} from 'rxjs/operators';

import { IdentityService } from './identity.service';
import { Config } from "./../config";

export interface IServerError {
    error: string;
    error_description: string;
}

@Injectable()
export class ApiService {

    constructor(
        public http: HttpClient,
        private identityService: IdentityService
    ) {
        
    }

    public get<T>(url: string, params?: object) {
        params = params
            ? Object.keys(params).reduce((p, k) => p.set(k, params[k]), new HttpParams())
            : {}
        return this.request<T>('GET', url, {params : params});
    }

    public post<T>(url: string, data?: object) {
        // data = data
        //     ? Object.keys(data).reduce((p, k) => p.set(k, data[k]), new HttpParams())
        //     : {}
        return this.request<T>('POST', url, {body : data});
    }

    public put<T>(url: string, data?: object) {
        // data = data
        //     ? Object.keys(data).reduce((p, k) => p.set(k, data[k]), new HttpParams())
        //     : {}
        return this.request<T>('PUT', url, {body : data});
    }

    public request<T>(method: string, url: string, options?: any): any {
        const absoluteUrl = Config.apiUrl + url;
        const headers = this.authHeader();

        options = Object.assign({
            body: null,
            params: null,
            responseType: "json",
            observe: "body",
            headers: headers,
        }, options);
        return this.http.request<T>(method, absoluteUrl, options)
            // .catch(this.handleErrors);
    }
    authHeader() {
        return new HttpHeaders({
            "Content-Type": "application/json",
            // "Access-Control-Allow-Origin": "localhost:40000",
            "Authorization": "bearer " + this.identityService.accessToken
        });
    }

    // handleErrors(error: HttpErrorResponse) {
    //     console.log(JSON.stringify(error));
    //     return Observable.throw(error.error);
    // }
}