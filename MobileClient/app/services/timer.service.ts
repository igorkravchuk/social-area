import { Injectable } from "@angular/core";
import { setTimeout, clearTimeout, } from 'tns-core-modules/timer';

@Injectable()
export class TimerService {

    timer: any;
    timeInterval:number = 10000;

    public enableTimer(func: () => Promise<any>) {
        this.updatingTimer(func);
    }

    public stopTimer() {
        clearTimeout(this.timer);
        this.timer = null;
    }

    private updatingTimer(func: () => Promise<any>): void {
        this.stopTimer();

        this.timer = setTimeout(() => {
            func().then(() => this.updatingTimer(func))
        }, this.timeInterval);
    }
}