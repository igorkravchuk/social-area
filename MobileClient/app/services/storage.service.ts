import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";
import { startWith, filter } from "rxjs/operators";
import { Person } from "~/models/Person";
import { Checkin } from "~/models/Checkin";
import { CheckinCategory } from "~/models/CheckinCategory";
import { Location } from "~/models/Location";

export enum StorageKeys {
    PERSONS,
    CHECKINS,
    CHECKIN_CATEGORIES,
    CURRENT_USER,
    CURRENT_LOCATION
}

@Injectable()
export class StorageService {

    private storageData: any = {};
    private storageSubjects: any = {};

    constructor() {
        Object.keys(StorageKeys).forEach(key => {
            this.storageData[key] = null;
            this.storageSubjects[key] = new Subject();
        })
    }

    private subscription(key: StorageKeys) {
        return this.storageSubjects[key]
            .asObservable()
            .pipe(
                startWith(this.storageData[key]),
                filter(k => k !== null && k !== undefined ));
    }

    private updateData(key: StorageKeys, data: any) {
        this.storageData[key] = data;
        this.storageSubjects[key].next(data);
    }



    public get persons(): Observable<Person[]> {
        return this.subscription(StorageKeys.PERSONS);
    }

    public updatePersons(data: Person[]) {
        this.updateData(StorageKeys.PERSONS, data);
    }

    public get checkins(): Observable<Checkin[]> {
        return this.subscription(StorageKeys.CHECKINS);
    }

    public updateCheckins(data: Person[]) {
        this.updateData(StorageKeys.CHECKINS, data);
    }

    public get checkinCategories(): Observable<CheckinCategory[]> {
        return this.subscription(StorageKeys.CHECKIN_CATEGORIES);
    }

    public updateCheckinCategories(data: CheckinCategory[]) {
        this.updateData(StorageKeys.CHECKIN_CATEGORIES, data);
    }

    public get currentLocation(): Observable<Location> {
        return this.subscription(StorageKeys.CURRENT_LOCATION);
    }

    public updateCurrentLocation(data: Location) {
        this.updateData(StorageKeys.CURRENT_LOCATION, data);
    }
    
    public get currentUser(): Observable<Person> {
        return this.subscription(StorageKeys.CURRENT_USER);
    }

    public updateCurrentUser(data: Person) {
        this.updateData(StorageKeys.CURRENT_USER, data);
    }
}