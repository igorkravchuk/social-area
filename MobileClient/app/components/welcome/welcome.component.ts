import { Component, OnInit } from "@angular/core";
import { PeopleRepository } from "~/repositories/people.repository";
import { LocalStorageService, LocalStorageKeys } from "~/services/local-storage.service";
import { RouterExtensions } from "nativescript-angular/router";
import { IdentityService } from "~/services/identity.service";
import { User } from "~/models/User";

@Component({
    selector: "WelcomeComponent",
    moduleId: module.id,
    templateUrl: "./welcome.component.html"
})

export class WelcomeComponent implements OnInit {
    user: User = new User();
    accessToken: string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0OGU4Yjk4MS1kOWE2LTQ3ZTQtODEyYi1hZDc4NTRhZjAxNWEiLCJqdGkiOiIwYWEyZDgxMy02ODUwLTQyZDYtOTY5ZS04YTk2N2UwZGFmN2MiLCJuYmYiOjE1MjgyNzQ0NjMsImV4cCI6MTUzMzQ1ODQ2MywiaXNzIjoic29jaWFsYXBwLmNvbSIsImF1ZCI6InNvY2lhbC1hcHAifQ.OcvA3U6ycdzSD_7ht3hhmkfEppiFY1xEMbIXcr_e65o";
    isLoading: boolean;

    constructor(
        private peopleRepository: PeopleRepository,
        private routerExtensions: RouterExtensions,
        private identityService: IdentityService
    ) {
    }

    ngOnInit(): void {
        if (this.identityService.isExist) {
            this.enter();
        }
    }
     
    public goToFacebook(): void {
        this.routerExtensions.navigate(['/register/facebook'])
    }

    public goToLogin(): void {
        this.routerExtensions.navigate(['/login'])
    }

    public goToRegistration(): void {
        this.routerExtensions.navigate(['/register'])
    }

    public enter() {
        this.routerExtensions.navigate(['/dashboard'], {clearHistory: true});
    }
}
