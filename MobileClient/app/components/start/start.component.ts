import { Component, OnInit, Input } from "@angular/core";
import { IdentityService } from "~/services/identity.service";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator";

@Component({
    selector: "StartComponent",
    moduleId: module.id,
    templateUrl: "./start.component.html"
})
export class StartComponent implements OnInit {
    isLoading: boolean;
    
    constructor(
        private routerExtensions: RouterExtensions,
        private identityService: IdentityService
    ) {
        this.isLoading = true;
    }
    
    ngOnInit(): void {
        if (this.identityService.isExist) {
            this.enter();
            this.isLoading = false;
        } else{
            this.toWelcome();
            this.isLoading = false;
        }
    }

    public enter() {
        this.routerExtensions.navigate(['/dashboard'], {clearHistory: true});
    }
    public toWelcome() {
        this.routerExtensions.navigate(['/welcome'], {clearHistory: true});
    }
}