import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { LocalStorageKeys, LocalStorageService } from "~/services/local-storage.service";
import { Person } from "~/models/Person";
import { PeopleService } from "~/services/people.service";
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator";
import { StorageService } from "~/services/storage.service";
import { CheckinService } from "~/services/checkin.service";
import { TimerService } from "~/services/timer.service";
import { NavigationService } from "~/services/navigation.service";
// import { Observable } from "tns-core-modules/data/observable";
// import frame = require("tns-core-modules/ui/frame");
// import { RadSideDrawer } from "nativescript-ui-sidedrawer";

@Component({
    selector: "DashboardComponent",
    moduleId: module.id,
    templateUrl: "./dashboard.component.html"
})

export class DashboardComponent implements OnInit {

    public userId: string;
    public people: Person[] = [];
    currentLocation: string;
    isLoading: boolean = false;

    public selectedIndex: number = 0;
    // sideDrawer: RadSideDrawer = <RadSideDrawer>( frame.topmost().getViewById("sideDrawer"));

    constructor(
        private localStorage: LocalStorageService,
        private routerExtensions: RouterExtensions,
        private peopleService: PeopleService,
        private storageService: StorageService,
        private checkInService: CheckinService,
        private timerService: TimerService,
        private navigationService: NavigationService
    ) {
    }

    ngOnInit(): void {
        this.timerService.enableTimer(this.updateData.bind(this));
        this.updateData();

        this.storageService.currentLocation.subscribe(location => {
            this.currentLocation = location.latitude + ',' + location.longitude;
        });

        this.navigationService.navigate.subscribe((pageNumber: number) => {
            this.selectedIndex = pageNumber
        })
    }

    public logOut(): void {
        this.routerExtensions.navigate(['/register']);
    }

    public updateData(): Promise<any> {
        this.isLoading = true;
        return Promise.all([
            this.peopleService.updateMyLocation(),
            this.peopleService.updatePersonsList(),
            this.checkInService.updateChekinsList(),
            this.checkInService.updateCheckinCategoriesList()
        ]).then(() => {
            this.isLoading = false;
        });
    }

    // tabViewIndexChange(index: number) {
    //     switch(index) {
    //       case 0: 
    //         this.routerExtensions.navigate([
    //         '/dashboard', { outlets: { map: ['map'] } }]);
    //         break;
    //     }
    //   }
}


