import { OnInit, Component } from "@angular/core";
import { Checkin } from "~/models/Checkin";
import { CheckinService } from "~/services/checkin.service";
import { RouterExtensions } from "nativescript-angular/router";
import { CheckinRequest } from "~/repositories/checkin.repository";
import { LocationService } from "~/services/location.service";
import { ListPicker } from "ui/list-picker";
import { StorageService } from "~/services/storage.service";
import { CheckinCategory } from "~/models/CheckinCategory";
import { LocalStorageService } from "~/services/local-storage.service";
import { SelectedIndexChangedEventData, ValueList, DropDown } from "nativescript-drop-down";
import { NavigationService } from "~/services/navigation.service";

@Component({
    selector: "CreateCheckinComponent",
    moduleId: module.id,
    templateUrl: "./create-checkin.component.html"
})

export class CreateCheckinComponent implements OnInit {

    public note: string;
    public categoryId: string;

    public isLoading: boolean = false

    lastClickTime: Date = new Date();
    clickInterval: number = 10;

    public checkinCategoriesNames: string[] = [];
    public checkinCategories:CheckinCategory[] = [];

    public formattedAddress: string;
    public formattedAddresses: string[] = [];

    public addressIndex: number;
    public categoryIndex: number;

    constructor(
        private checkInService: CheckinService,
        private routerExtensions: RouterExtensions,
        private locationService: LocationService,
        private storageService: StorageService,
        private navigationService: NavigationService
    )
    {
        
        
    }

    ngOnInit(): void {

        this.storageService.checkinCategories.subscribe((categories: any) => {
            this.checkinCategories = categories
            this.addCategories();
        });

        this.locationService.getFormettedAddress().then((address: any) => {
            address.forEach(item => {
                this.formattedAddresses.push(item.formatted_address);
            });
        });
    }

    addCategories(): void {
        this.checkinCategoriesNames = [];
        this.checkinCategories.forEach((category) => {
            this.checkinCategoriesNames.push(category.name);
        })
    }

    public sendCheckin(): void {
        if (this.availableNextClick()) {
            this.isLoading = true;
            this.locationService.currentLocation()
            .then((loc) => {
                let checkin = {
                    note : this.note,
                    locationLatitude: loc.latitude,
                    locationLangitude: loc.longitude,
                    formattedAddress: this.formattedAddress,
                    checkinCategoryId: this.categoryId
                }
                console.log(checkin.formattedAddress);
                return this.checkInService.createCheckin(checkin);
            })
            .then(() => {
                this.lastClickTime = new Date();
                alert("Checkin was succesfully added ");
                
                return this.checkInService.updateChekinsList();
            })
            .catch((error: any) => {
                alert(error);
            })
            .then(() => {
                this.note = "";
                this.isLoading = false;
                this.navigationService.changeDashboardPage(0);
                
            })
            
            
        } else {
            alert("Wait for 10 sec for make checkin");
        }
        // let tabView: TabView = <TabView>this.page.getViewById("tabView");
        // tabView.selectedIndex = 0;

    }
    availableNextClick(): boolean {
        if (!this.lastClickTime) return true;
        let timeDiff = (new Date().getTime() - this.lastClickTime.getTime()) / 1000;
        return timeDiff > this.clickInterval;

    }
    public selectedAddressIndexChanged(args: SelectedIndexChangedEventData) {
        console.log(args.newIndex);
        this.formattedAddress = this.formattedAddresses[args.newIndex];
    }
    public selectedCategoryIndexChanged(args: SelectedIndexChangedEventData) {

        let category = this.checkinCategoriesNames[args.newIndex];
        this.categoryId = this.checkinCategories.find(a=>a.name === category).checkinCategoryId;
    }
}