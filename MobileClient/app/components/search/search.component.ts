import { Component, OnInit, Input } from "@angular/core";
import { PeopleRepository } from '../../repositories/people.repository';
import { Person } from "~/models/Person";
import { LocationService } from "~/services/location.service";
import { PeopleService } from "~/services/people.service";
import { DashboardComponent } from "~/components/dashboard/dashboard.component";
import { LocalStorageService, LocalStorageKeys } from "~/services/local-storage.service";
import { RouterExtensions } from "nativescript-angular/router";
import { IdentityService } from "~/services/identity.service";
import { StorageService } from "~/services/storage.service";
import { SegmentedBar, SegmentedBarItem } from "ui/segmented-bar";
import { Checkin } from "~/models/Checkin";

@Component({
    selector: "SearchComponent",
    moduleId: module.id,
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {


    @Input() people: Person[] = [];
    checkins: Checkin[] = [];
    userId: string;
    public segmentedItems: Array<SegmentedBarItem>;
    public selectedSegmentedIndex = 0;
    public showPeople = true;
    public showCheckins = true;

    constructor(
        private peopleRepository: PeopleRepository,
        private locationService: LocationService,
        private peopleService: PeopleService,
        private localStorage: LocalStorageService,
        private routerExtensions: RouterExtensions,
        private identityService: IdentityService,
        private storageService: StorageService
    ) {
        this.segmentedItems = [];
        let peopleSegmentedBarItem = <SegmentedBarItem> new SegmentedBarItem();
        peopleSegmentedBarItem.title = "People";
        this.segmentedItems.push(peopleSegmentedBarItem);
        let checkinsSegmentedBarItem = <SegmentedBarItem> new SegmentedBarItem();
        checkinsSegmentedBarItem.title = "Checkins";
        this.segmentedItems.push(checkinsSegmentedBarItem);

    }

    ngOnInit(): void {

        this.storageService.persons.subscribe((persons) => {
            this.people = persons;
        });
        this.storageService.checkins.subscribe((checkins) => {
            this.checkins = checkins;
        });
    }

    public openProfile(userId: number): void {
        this.routerExtensions.navigate(['/detail', userId]);
    }

    public onSelectedIndexChange(args) {
        let segmetedBar = <SegmentedBar>args.object;
        this.selectedSegmentedIndex = segmetedBar.selectedIndex;
        switch (this.selectedSegmentedIndex) {
            case 0:

                this.showPeople = true;
                this.showCheckins = false;
                break;
            case 1:
                this.showPeople = false;
                this.showCheckins = true;
                break;
            default:
                break;
        }
    }
    public getFacebookPicture(personId: string) {
        let facebookId = "https://graph.facebook.com/me?fields=id&access_token=\""+ this.identityService.accessToken +"\"";

        if(facebookId) {
            return "http://graph.facebook.com/" + facebookId + "/picture?type=square";
        } else {
            return "http://www.divestco.com/wp-content/uploads/2017/08/person-placeholder-portrait-300x300.png";
        }
    }

    public getFormattedAddress(latitude: number, longitude: number) {
        this.locationService.getFormattedAddressByLatLong(latitude, longitude)
            .then((result: any) => {
                return result;
            }).catch((error: any) => {
                return error;
            });
    }
}

