import { Component, Input, OnChanges } from "@angular/core";
import { Config } from "../../config";

@Component({
    selector: "MapPreviewComponent",
    moduleId: module.id,
    template: '<Image [src]="imagePreviewUrl" stretch="aspectFill" height="100%" (tap)="onTap()" *ngIf="imagePreviewUrl"></Image>',
})
export class MapPreviewComponent implements OnChanges {
    @Input() address;
    @Input() zoom = 15;
    @Input() width = 500;
    @Input() height = 500;
    @Input() markers = [];
    private url = "https://maps.googleapis.com/maps/api/staticmap";
    imagePreviewUrl: string;

    // private locator = new LocateAddress();

    ngOnChanges() {
        this.imagePreviewUrl = this.address ? this.getImagePreviewUrl() : '';
    }

    getImagePreviewUrl(): string {
        let params = {
            center: this.address,
            zoom: this.zoom,
            size: this.width + "x" + this.height,
            key: Config.googleMapKey,
            markers: ["color:blue"].concat(this.markers).join('|')
        }
        let searchParams = Object.keys(params).map(k => `${k}=${params[k]}`).join("&");
        let url = this.url + "?" + searchParams;
        return encodeURI(url);
    }

    onTap() {
        // this.locator.locate({
        //     address: this.address
        // })
    }
}
