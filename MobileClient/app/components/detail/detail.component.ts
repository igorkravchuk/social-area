import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { PeopleRepository } from '../../repositories/people.repository';
import { Person } from "~/models/Person";
import * as utilityModule from "utils/utils";

@Component({
    selector: "DetailComponent",
    moduleId: module.id,
    templateUrl: "./detail.component.html"
})

export class DetailComponent implements OnInit {
    person: Person = null;

	public constructor (
        private routerExtensions: RouterExtensions,
        private peopleRepository: PeopleRepository, 
        private route: ActivatedRoute
    ) { }
    
    ngOnInit(): void {
        const id = this.route.snapshot.params.id;
        this.peopleRepository.getPerson(id).then((result: Person) => {
            this.person = result;
        })
     }
    
    public onBack() {
        this.routerExtensions.back();
    }

    public launchInsta(link: string) {
        utilityModule.openUrl("https://www.instagram.com/" + link);
    }
}
