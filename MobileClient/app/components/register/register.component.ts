import { Component, OnInit } from "@angular/core";
import { PeopleRepository } from "~/repositories/people.repository";
import { LocalStorageService, LocalStorageKeys } from "~/services/local-storage.service";
import { RouterExtensions } from "nativescript-angular/router";
import { IdentityService } from "~/services/identity.service";
import { User } from "~/models/User";


@Component({
    selector: "RegisterComponent",
    moduleId: module.id,
    templateUrl: "./register.component.html"
})



export class RegisterComponent implements OnInit {
    user: User;
    isLoading: boolean = false;
    // accessToken: string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0OGU4Yjk4MS1kOWE2LTQ3ZTQtODEyYi1hZDc4NTRhZjAxNWEiLCJqdGkiOiIwYWEyZDgxMy02ODUwLTQyZDYtOTY5ZS04YTk2N2UwZGFmN2MiLCJuYmYiOjE1MjgyNzQ0NjMsImV4cCI6MTUzMzQ1ODQ2MywiaXNzIjoic29jaWFsYXBwLmNvbSIsImF1ZCI6InNvY2lhbC1hcHAifQ.OcvA3U6ycdzSD_7ht3hhmkfEppiFY1xEMbIXcr_e65o";
    
    constructor(
        private peopleRepository: PeopleRepository,
        private localStorage: LocalStorageService,
        private routerExtensions: RouterExtensions,
        private identityService: IdentityService
    ) {
        this.user = new User()
    }

    ngOnInit(): void {
        // let userId = this.localStorage.getItem(LocalStorageKeys.USER);
        // if (userId) {
        //     this.routerExtensions.navigate(['/dashboard']);
        // }
     }

    public register(): void {
        this.isLoading = true;
        this.peopleRepository.register(this.user.userName, this.user.password)
            .then((result: any) => {
                this.localStorage.setItem(LocalStorageKeys.ACCESS_TOKEN, result.token)
                this.routerExtensions.navigate(['/profile'], {clearHistory: true});
                this.isLoading = false;
            }).catch((error: Error) => {
                alert(error.message);
                this.isLoading = false;
            })
    }
    
    public onBack(): void {
        this.routerExtensions.back();
    }
}
