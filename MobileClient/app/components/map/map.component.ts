import { Component, OnInit, OnChanges, Input, ElementRef, ViewChild, ViewChildren } from "@angular/core";
import { PeopleRepository } from '../../repositories/people.repository';
import { Person } from "~/models/Person";
import { LocationService } from "~/services/location.service";
import { PeopleService } from "~/services/people.service";

import { registerElement } from "nativescript-angular/element-registry";
import { StorageService } from "~/services/storage.service";
import { Mapbox } from "nativescript-mapbox"
import { RouterExtensions } from "nativescript-angular/router";
import { Checkin } from "~/models/Checkin";
import { Config } from "~/config";
import { map, debounce } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';
import { timer } from 'rxjs/observable/timer';


@Component({
    selector: "MapComponent",
    moduleId: module.id,
    templateUrl: "./map.component.html"
})

export class MapComponent implements OnInit {
    people: Person[] = [];
    checkins: Checkin[] = [];
    @Input() currentUser: Person;
    @Input() isLoading: boolean = true;



    @ViewChild("map") public mapbox: ElementRef;

    peopleLocation: string[] = [];
    mapView: Mapbox;
    latitude: number = 0;
    longitude: number = 0;
    zoom: number = 15;
    markersIds: number[] = [];
    mapboxToken = Config.mapboxToken;

    constructor(
        private routerExtensions: RouterExtensions,
        private peopleRepository: PeopleRepository,
        private locationService: LocationService,
        private peopleService: PeopleService,
        private storageService: StorageService
    ) {
    }

    ngOnInit(): void {

        merge(
            this.storageService.persons.pipe(map(persons => {
                this.people = persons;
            })),

            this.storageService.checkins.pipe(map(checkins => {
                this.checkins = checkins;
            })),

            this.storageService.currentLocation.pipe(map(location => {
                
            }))
        )
        .pipe(debounce(() => timer(1000)))
        .subscribe(() => {
            this.buildMarkers();
        })
        
    }
    public buildMarkers(): void {
        if (!this.currentUser) return;
        
        if (this.mapView) {
            this.mapView.removeMarkers(this.markersIds);
            this.markersIds.length = 0;            

            this.checkins.forEach((checkin) => {
                this.addCheckinMarker(checkin)
            })

            this.people.forEach((person) => {
                if (person.personId == this.currentUser.personId) return;
                this.addPersonMarker(person);
            });

            this.addMeMarker();
        }
    }

    public addPersonMarker(person: Person): void {
        this.addMarker({
            lat: person.location.latitude,
            lng: person.location.longitude,
            title: person.profile.userName,
            iconPath: "res/markers/user.png",
            onCalloutTap: () => this.openProfile(person.personId)
        });
    }
    
    public addCheckinMarker(checkin: Checkin): void {
        this.addMarker({
            lat: checkin.location.latitude,
            lng: checkin.location.longitude,
            title: checkin.note,
            subtitle: checkin.formattedAddress,
            iconPath: "res/markers/flag.png",
            selected: true
            // onCalloutTap: () => this.openProfile(person.personId)
        });
    }

    public addMeMarker() {
        this.addMarker({
            lat: this.currentUser.location.latitude,
            lng: this.currentUser.location.longitude,
            title: this.currentUser.profile.userName,
            iconPath: "res/markers/me.png"
            // onCalloutTap: () => this.openProfile(person.personId)
        });
    }
    public addMarker(options) {
        let markerId = this.markersIds.length + 1;
        options.id = markerId;
        this.markersIds.push(markerId);
        this.mapView.addMarkers([options]);
    }

    public openProfile(userId: number): void {
        this.routerExtensions.navigate(['/detail', userId]);
    }

    onMapReady(event) {
        this.mapView = event.map;
        this.buildMarkers();

        this.peopleRepository.getCurrentUser().then((person: Person) => {
            this.currentUser = person;
            this.navigateToMyLocation();
            this.buildMarkers();
        })
    }


    navigateToMyLocation() {
        if (this.currentUser) {
            this.mapView.setCenter({
                lat: this.currentUser.location.latitude,
                lng: this.currentUser.location.longitude,
                animated: true
            });
        }
    }

    // onCoordinateTapped(args) {
    //     console.log("Coordinate Tapped, Lat: " + args.position.latitude + ", Lon: " + args.position.longitude, args);
    // }

    // onMarkerEvent(args) {
    //     console.log("Marker Event: '" + args.eventName
    //         + "' triggered on: " + args.marker.title
    //         + ", Lat: " + args.marker.position.latitude + ", Lon: " + args.marker.position.longitude, args);
    // }
}
