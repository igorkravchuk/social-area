import { Component, OnInit } from "@angular/core";
import { PeopleRepository } from "~/repositories/people.repository";
import { LocalStorageService, LocalStorageKeys } from "~/services/local-storage.service";
import { RouterExtensions } from "nativescript-angular/router";
import { IdentityService } from "~/services/identity.service";
import { User } from "~/models/User";

@Component({
    selector: "LoginComponent",
    moduleId: module.id,
    templateUrl: "./login.component.html"
})

export class LoginComponent implements OnInit {
    user: User = new User();
    accessToken: string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0OGU4Yjk4MS1kOWE2LTQ3ZTQtODEyYi1hZDc4NTRhZjAxNWEiLCJqdGkiOiIwYWEyZDgxMy02ODUwLTQyZDYtOTY5ZS04YTk2N2UwZGFmN2MiLCJuYmYiOjE1MjgyNzQ0NjMsImV4cCI6MTUzMzQ1ODQ2MywiaXNzIjoic29jaWFsYXBwLmNvbSIsImF1ZCI6InNvY2lhbC1hcHAifQ.OcvA3U6ycdzSD_7ht3hhmkfEppiFY1xEMbIXcr_e65o";
    isLoading: boolean;

    constructor(
        private peopleRepository: PeopleRepository,
        private routerExtensions: RouterExtensions,
        private identityService: IdentityService
    ) {
        // this.user.userName = "test";
        // this.user.password = "P@ssword1";
    }

    ngOnInit(): void {
    }
    
    public logIn(): void {
        this.isLoading = true;
        this.peopleRepository.login(this.user.userName, this.user.password)
            .then((result: any) => {
                this.identityService.accessToken = result.token;
                this.enter();
                this.isLoading = false;
            }).catch((error: any) => {
                console.log(error);
                alert('Login Error: ' + error.error);
                this.isLoading = false;
            });
    }

    public onBack(): void {
        this.routerExtensions.back();
    }
    
    public enter() {
        this.routerExtensions.navigate(['/dashboard'], {clearHistory: true});
    }
}
