import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { IdentityService } from "~/services/identity.service";
import { User } from "~/models/User";
import { Profile } from "~/models/Profile";
import { PeopleRepository } from "~/repositories/people.repository";
import { Person } from "~/models/Person";
import { LocationService } from "~/services/location.service";


@Component({
    selector: "ProfileComponent",
    moduleId: module.id,
    templateUrl: "./profile.component.html"
})

export class ProfileComponent implements OnInit {
    // profile: Profile
    accessToken: string;
    isEditing: boolean = false;
    isEnabledLocation: boolean = true;
    genderList: Array<string> = ["Male", "Female", "Don't want to say"];
    isLoading: boolean;
    personId: string = null;
    person: Person = null;

	public constructor (
        private routerExtensions: RouterExtensions,
        private identityService: IdentityService,
        private peopleRespository: PeopleRepository,
        private locationService: LocationService
    ) {
        this.person = new Person();
     }

    ngOnInit(): void {
        this.peopleRespository.getCurrentUser().then((result) => {
            this.person = result;
            this.person.profile.name = this.person.profile.name || this.person.profile.userName;
            this.person.profile.instagram = this.person.profile.instagram;
            // this.person.profile.name = result.profile.name;
            // this.person.profile.gender = result.profile.gender;
        }).catch((error: Error) => {
            alert('Location Error: ' + error.message);
        })
    }

    public onBack() {
        this.routerExtensions.back();
    }

    public onEdit() {
        this.isEditing = true;
    }

    public onSave() {
        this.isEditing = false;
        this.peopleRespository.editUserProfile(this.person)
            .then((result) => {
                alert("Profile has been changed");
            }).catch((error: Error) => {
                alert(error.message);
            })
    }

    public onCancel() {
        this.isEditing = false;
    }

    public changeProfilePicture() {
    }

    checkEnableLocation() {
        if (this.isEnabledLocation){

        } else {
            
        }
    }

    getCurrentLocation() {
        this.isLoading = true;
        this.locationService.currentLocation()
            .then((loc) => {
                let latitude = loc.latitude;
                let longitude = loc.longitude;
                this.isLoading = false;
                alert("Latitude: " + latitude + "\nLongitude: " + longitude);
            });
        // alert("Latitude: " + this.person.location.latitude + "\nLongitude: " + this.person.location.longitude);
    }

    public logOut() {
        this.identityService.clear();
        this.routerExtensions.navigate(['/start'], {clearHistory: true});
    }
}


