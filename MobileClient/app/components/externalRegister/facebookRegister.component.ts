import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { WebView, LoadEventData } from "ui/web-view";
import {Config} from "../../config";
import { Observable } from "tns-core-modules/data/observable";
import { IdentityService } from "~/services/identity.service";
import { DefaultUrlSerializer, UrlTree } from '@angular/router'
import { UrlService } from "~/services/url.service";
import { url } from "url";

@Component({
    selector: "FacebookRegisterComponent",
    moduleId: module.id,
    templateUrl: "./facebookRegister.component.html"
})


export class FacebookRegisterComponent implements OnInit {

    @ViewChild("webView") webView: ElementRef;
    public webViewSrc: string = Config.apiUrl + "/auth/signIn?provider=Facebook";
    
    constructor(
        private routerExtensions: RouterExtensions,
        private identityService: IdentityService,
        private urlService: UrlService
    ) {}

    ngOnInit(): void {
        let webview: WebView = this.webView.nativeElement;


        webview.on(WebView.loadFinishedEvent, (args: LoadEventData) => {
            var url = args.url;
            	
            var queryString = url.substring( url.indexOf('?'));
            var params = this.urlService.parseParams(queryString);
            var baseUrl = this.urlService.parseBaseUrl(url);
            var targeturl = Config.apiUrl + '/Auth/SignInSuccess';
            if (targeturl.toLowerCase() == baseUrl.toLowerCase() && params.token) {
                this.identityService.accessToken = params.token.substring(0, params.token.length - 4);
                this.routerExtensions.navigate(['/dashboard'], {clearHistory: true});
            }
        })
    }
    
    public onBack(): void {
        this.routerExtensions.back();
    }

    getUrlParams
}