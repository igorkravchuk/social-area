import { Component, OnInit } from "@angular/core";
import { SelectedIndexChangedEventData, TabView, TabViewItem } from "tns-core-modules/ui/tab-view";
import { NavigationEnd, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "app.component.html",
    styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
    constructor() {
    }

    ngOnInit(): void {
    }

    // getIconSource(icon: string): string {
    //     const iconPrefix = isAndroid ? "res://" : "res://tabIcons/";

    //     return iconPrefix + icon;
    // }
}
