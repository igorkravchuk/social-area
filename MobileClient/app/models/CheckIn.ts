import { Location } from "~/models/Location";
import { Person } from "~/models/Person";
import { CheckinCategory } from "~/models/CheckinCategory"

export class Checkin {
    checkinId: string;
    location?: Location;
    person?: Person;
    note: string;
    created: string;
    checkinCategory?: CheckinCategory;
    checkinCategoryId: string;
    formattedAddress: string;
}