export class Profile {
    userName: string;
    facebookId: string;
    name: string;
    gender: string;
    instagram: string;
    facebook: string;
    pictureUrl: string;
}