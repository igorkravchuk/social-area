import { Profile } from "~/models/Profile";
import { Location } from "~/models/Location";

export class Person {
    public personId: number;
    public name: string;
    public desc: string;
    public pictureUrl: string;
    public instagram: string;
    public facebook: string;
    public locationLatitude: number;
    public locationLongitude: number;
    public distance: number;

    public location?: Location;
    public profile?: Profile;

}