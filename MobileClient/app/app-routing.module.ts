import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { StartComponent } from "./components/start/start.component";
import { WelcomeComponent } from "./components/welcome/welcome.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { FacebookRegisterComponent } from "./components/externalRegister/facebookRegister.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { SearchComponent } from "./components/search/search.component";
import { MapComponent } from "./components/map/map.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { DetailComponent } from "./components/detail/detail.component";
import { CreateCheckinComponent } from "~/components/create-checkin/create-checkin.component";

export const COMPONENTS = [StartComponent, WelcomeComponent, LoginComponent, RegisterComponent, FacebookRegisterComponent, 
    DashboardComponent, SearchComponent, MapComponent, ProfileComponent, DetailComponent, CreateCheckinComponent];

const routes: Routes = [
    // { path: "", redirectTo: "/(homeTab:home//browseTab:browse//searchTab:search)", pathMatch: "full" },
    { path: "", redirectTo: "start", pathMatch: "full" },

    { path: "start", component: StartComponent },
    { path: "welcome", component: WelcomeComponent },
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "register/facebook", component: FacebookRegisterComponent },
    { path: "dashboard", component: DashboardComponent,
    children: [
        { path: "", redirectTo: "/dashboard/(search:search//map:map//checkIn:checkIn)", pathMatch: "full" },
        { path: "search", component: SearchComponent, outlet: "search" },
        { path: "map", component: MapComponent, outlet: "map" },
        { path: "checkIn", component: CreateCheckinComponent, outlet: "checkIn"}
    ] },
    { path: "profile", component: ProfileComponent },
    { path: "detail/:id", component: DetailComponent },
    { path: "map", component: MapComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
